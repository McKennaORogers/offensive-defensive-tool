import socketserver, binascii

host = "0.0.0.0"
port = 8081

class TcpServer(socketserver.BaseRequestHandler):
	def handle(self):
		data = self.request.recv(512).strip()
		if(data.startswith(b"send:")):
			fle = data.split(b":")[1]
			print("Receiving file:")
			with open(str(fle.decode("utf-8")), "ab") as f:
				while(data):
					data = bytearray(self.request.recv(512).strip())
					if(len(data) % 2 == 0):
						f.write(binascii.unhexlify(data))
					else:
						f.write(binascii.unhexlify(data.append(0)))
		print("File received")

server = socketserver.TCPServer(("", port), TcpServer)
print("[Server started]")
server.serve_forever()
