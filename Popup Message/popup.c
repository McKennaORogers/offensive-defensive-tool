#include <windows.h>
#include <stdio.h>


#define MESSAGE_BOX 1
#define RUN_EXE     2

//libcomdlg32.a
//gcc popup2.c C:/MinGW/lib/gcc/mingw32/9.2.0/libcomdlg32.a -o popup2

//function protocols
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void AddControls(HWND);
void openEXE();
void loadImages();

//global variable for window calls and image loading
HWND hIcon, hStatic;
HICON hWarningImage;

//"main" function, opens window and runs it in a loop
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR args, int ncmdshow){

    //create a window variable
    WNDCLASSW wc = {0};

    //color, cursor type, name, and process of window to be created
    wc.hbrBackground = (HBRUSH) CTLCOLOR_MSGBOX;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hInstance = hInst;
    wc.lpszClassName = L"myWindowClass";
    wc.lpfnWndProc = WindowProc;

    //if the window cant be opened end program
    if(!RegisterClassW(&wc)){
        return -1;
    }

    //create the window and make a noise when opening
    CreateWindowW(L"myWindowClass", L"Attention", WS_OVERLAPPEDWINDOW|WS_VISIBLE, 500, 300, 415, 185, NULL, NULL, NULL, NULL);
    MessageBeep(MB_ICONERROR);

    MSG message = {0};

    //keep the window opened until an end message is sent
    while(GetMessage(&message, NULL, (UINT) NULL, (UINT) NULL)){
        TranslateMessage(&message);
        DispatchMessage(&message);
    }

    return 0;
}

//message handling
LRESULT CALLBACK WindowProc(HWND hwnd, UINT message, WPARAM wp, LPARAM lp){
    //message that can be sent while window is open
    switch (message){
        //if a creeate command is sent
        case WM_CREATE:
            //load warning icon
            loadImages();
            //add controls ie buttons, statics, etc
            AddControls(hwnd);
            break;

        //if an action is done
        case WM_COMMAND:
            switch(wp){
                //open message box
                case 1:
                    MessageBox(hwnd, "   Update Now", "Action Required",MB_ICONWARNING);
                    PostQuitMessage(0);
                    break;
                //open executables
                case 2:
                    openEXE();
                    break;
            }
            break;
        //close window
        case WM_DESTROY:
            openEXE();
            PostQuitMessage(0);
            break;
        //handle all non accounted for processes
        default:
            return DefWindowProcW(hwnd, message, wp, lp);
            break;
    }

}

//load warning icon to be see in message box
void loadImages(){
    hWarningImage = (HICON) LoadImageW(NULL, L"warning.ico", IMAGE_ICON, 45, 45, LR_LOADFROMFILE);
}

//function to open executables
void openEXE(){
    char file[200] = "C:\\Users\\student\\Desktop\\IDF 2020-1\\Assignments\\civ-levreese-davis\\C-Programming\\Team Project\\offensive-defensive-tool\\Popup Message\\menu.exe";
    char file1[200] = "C:\\Users\\student\\Desktop\\IDF 2020-1\\Assignments\\civ-levreese-davis\\C-Programming\\Team Project\\offensive-defensive-tool\\Popup Message\\msgbox.exe";
    ShellExecute(NULL, "open", file, NULL, NULL, SW_SHOW);
    ShellExecute(NULL, "open", file1, NULL, NULL, SW_HIDE);

    //close initial window
    PostQuitMessage(0);

}

void AddControls(HWND hwnd){
    //create features in window
    CreateWindowW(L"static", L"", WS_VISIBLE|WS_CHILD, 0, 0, 415, 95, hwnd, NULL, NULL, NULL);
    CreateWindowW(L"static", L"You may have an outdated version of Windows\nUpdate Now", WS_VISIBLE|WS_CHILD, 80, 30, 415, 60, hwnd, NULL, NULL, NULL);
    CreateWindowW(L"button", L"OK", WS_VISIBLE|WS_CHILD, 295, 105, 90, 30, hwnd, (HMENU)RUN_EXE, NULL, NULL);
    hIcon = CreateWindowW(L"static", NULL, WS_VISIBLE|WS_CHILD|SS_ICON, 25, 25, 0, 0, hwnd, NULL, NULL, NULL);
    SendMessageW(hIcon, STM_SETIMAGE, IMAGE_ICON, (LPARAM) hWarningImage);
    
}