
#include <stdio.h>

#ifdef _WIN32
    #include <Windows.h>

#elif __linux__
//use gcc <name>.c -o <alias> `pkg-config --cflags --libs gtk+-3.0`
    #include <gtk/gtk.h>

#endif


int main(int argc, char *argv[]){
    #ifdef _WIN32
        MessageBoxA( NULL, "  Running EXE", "Warning", MB_ICONWARNING );

    #elif __linux__
        GtkWidget *window;
        GtKWidget *button;
        gtk_init (&argc, &argv);

        window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_container_set_border_width(GTK_CONTAINER(window), 50);
        g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

        button = gtk_button_new_with_label("OK");
        g_signal_connect(GTK_OBJECT(button), "clicked", G_CALLBACK(greet), "button");
        gtk_container_add(GTK_CONTAINER(window), button);

        gtk_widget_show_all(window);
        g_print("Window Created\n");

        gtk_main();
        
    #endif
        return 0;
}
