
#include <windows.h>
#include <stdio.h>

double cost=0;
char sum[100];

/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {


	switch(Message) {
		

		case WM_CREATE: {
			
			//Creating a Button 11 parameters
			CreateWindow(TEXT("STATIC"),TEXT("APPETIZER"),
			 WS_VISIBLE|WS_CHILD,40,10,100,50, hwnd,(HMENU) 1, NULL,NULL 
			 );
			 CreateWindow(TEXT("STATIC"),TEXT("SOUP/SALAD"),
			 WS_VISIBLE|WS_CHILD,40,90,100,50, hwnd,(HMENU) 1, NULL,NULL 
			 );
			 CreateWindow(TEXT("STATIC"),TEXT("ENTREE"),
			 WS_VISIBLE|WS_CHILD,40,170,100,50, hwnd,(HMENU) 1, NULL,NULL 
			 );
			 CreateWindow(TEXT("STATIC"),TEXT("DESSERT"),
			 WS_VISIBLE|WS_CHILD,40,250,100,50, hwnd,(HMENU) 1, NULL,NULL 
			 );
			 CreateWindow(TEXT("STATIC"),TEXT("DRINK"),
			 WS_VISIBLE|WS_CHILD,40,330,100,50, hwnd,(HMENU) 1, NULL,NULL 
			 );
			 CreateWindow(TEXT("STATIC"),TEXT("CLICK BELOW FOR YOUR TOTAL:"),
			 WS_VISIBLE|WS_CHILD,40,430,700,50, hwnd,(HMENU) 1, NULL,NULL 
			 );			 
			 
			 
			/*Appetizer*/ 
			CreateWindow(TEXT("BUTTON"),TEXT("Wings"),
			 WS_VISIBLE|WS_CHILD,35,40,100,30, hwnd,(HMENU) 1, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Cheese Sticks"),
			 WS_VISIBLE|WS_CHILD,160,40,100,30, hwnd,(HMENU) 2, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Nachos"),
			 WS_VISIBLE|WS_CHILD,280,40,100,30, hwnd,(HMENU) 3, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Sliders"),
			 WS_VISIBLE|WS_CHILD,400,40,100,30, hwnd,(HMENU) 4, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Empanadas"),
			 WS_VISIBLE|WS_CHILD,520,40,100,30, hwnd,(HMENU) 5, NULL,NULL
			 );			

			/*Soup/Salad*/
			CreateWindow(TEXT("BUTTON"),TEXT("Caesar"),
			 WS_VISIBLE|WS_CHILD,35,120,100,30, hwnd,(HMENU) 6, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Garden"),
			 WS_VISIBLE|WS_CHILD,160,120,100,30, hwnd,(HMENU) 7, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Chili"),
			 WS_VISIBLE|WS_CHILD,280,120,100,30, hwnd,(HMENU) 8, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Chicken"),
			 WS_VISIBLE|WS_CHILD,400,120,100,30, hwnd,(HMENU) 9, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Tomato"),
			 WS_VISIBLE|WS_CHILD,520,120,100,30, hwnd,(HMENU) 10, NULL,NULL
			 );						 
			 
			 /*Entree*/
			 CreateWindow(TEXT("BUTTON"),TEXT("Burger"),
			 WS_VISIBLE|WS_CHILD,35,200,100,30, hwnd,(HMENU) 11, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Club Sandwich"),
			 WS_VISIBLE|WS_CHILD,160,200,100,30, hwnd,(HMENU) 12, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Meatloaf"),
			 WS_VISIBLE|WS_CHILD,280,200,100,30, hwnd,(HMENU) 13, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Fried Chicken"),
			 WS_VISIBLE|WS_CHILD,400,200,100,30, hwnd,(HMENU) 14, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Lasagna"),
			 WS_VISIBLE|WS_CHILD,520,200,100,30, hwnd,(HMENU) 15, NULL,NULL
			 );			
			 
			 /*Dessert*/
			 CreateWindow(TEXT("BUTTON"),TEXT("Cake"),
			 WS_VISIBLE|WS_CHILD,35,280,100,30, hwnd,(HMENU) 16, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Apple Pie"),
			 WS_VISIBLE|WS_CHILD,160,280,100,30, hwnd,(HMENU) 17, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Bread Pudding"),
			 WS_VISIBLE|WS_CHILD,280,280,100,30, hwnd,(HMENU) 18, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Ice Cream"),
			 WS_VISIBLE|WS_CHILD,400,280,100,30, hwnd,(HMENU) 19, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Cookies"),
			 WS_VISIBLE|WS_CHILD,520,280,100,30, hwnd,(HMENU) 20, NULL,NULL
			 );			
			 
			 /*Drinks*/
			 CreateWindow(TEXT("BUTTON"),TEXT("Coke"),
			 WS_VISIBLE|WS_CHILD,35,360,100,30, hwnd,(HMENU) 21, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Sprite"),
			 WS_VISIBLE|WS_CHILD,160,360,100,30, hwnd,(HMENU) 22, NULL,NULL 
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Tea"),
			 WS_VISIBLE|WS_CHILD,280,360,100,30, hwnd,(HMENU) 23, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Coffee"),
			 WS_VISIBLE|WS_CHILD,400,360,100,30, hwnd,(HMENU) 24, NULL,NULL
			 );
			CreateWindow(TEXT("BUTTON"),TEXT("Shake"),
			 WS_VISIBLE|WS_CHILD,520,360,100,30, hwnd,(HMENU) 25, NULL,NULL
			 );			

			/*Controls*/
			CreateWindow(TEXT("BUTTON"),TEXT("TOTAL"),
			 WS_VISIBLE|WS_CHILD,35,450,100,30, hwnd,(HMENU) 26, NULL,NULL
			 );	
			CreateWindow(TEXT("BUTTON"),TEXT("RESET"),
			 WS_VISIBLE|WS_CHILD,160,450,100,30, hwnd,(HMENU) 27, NULL,NULL
			 );				 			 
			 
			 /*Creating Text Box
			 CreateWindow(TEXT("EDIT"),TEXT(""),
			 WS_VISIBLE|WS_CHILD|WS_BORDER,
			 35,500,100,25,
			 hwnd,(HMENU)NULL,NULL,NULL);*/
			 
			 // Creating Menu
			HMENU hMenubar=CreateMenu();
			HMENU hFile=CreateMenu();
			HMENU hOptions=CreateMenu();
			
			AppendMenu(hMenubar,MF_POPUP,(UINT_PTR)hFile,"File");
			AppendMenu(hMenubar,MF_POPUP,(UINT_PTR)hOptions,"Options");
			
			AppendMenu(hFile, MF_STRING, NULL,"Exit");
			AppendMenu(hOptions, MF_STRING, NULL,"Full Menu");
			
			SetMenu(hwnd,hMenubar);
			
			break;
		}
		case WM_COMMAND:{
			//double cost;
			//char text[]="";
		//action performed on click of a button
		
			/*Appetizer*/		
			if(LOWORD(wParam)==1)//button 1 action
			{
				//Creating a Message Box
				cost=cost+5.45;
				sprintf(sum,"WINGS\nCost: $5.45\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Appetizer",MB_ICONINFORMATION);
					
			}
			if(LOWORD(wParam)==2)//button 2 action
			{
			//write a logic to add the total price and display in message box
			cost=cost+4.5;
			sprintf(sum,"CHEESE STICKS\nCost: $4.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Appetizer",MB_ICONINFORMATION);
			}
			if(LOWORD(wParam)==3)//button 3 action
			{
					cost=cost+5;
					sprintf(sum,"NACHOS\nCost: $5.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Appetizer",MB_ICONINFORMATION);
			}
			if(LOWORD(wParam)==4)//button 4 action
			{
					cost=cost+6;
					sprintf(sum,"SLIDERS\nCost: $6.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Appetizer",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==5)//button 5 action
			{
					cost=cost+5;
					sprintf(sum,"EMPANADAS\nCost: $5.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Appetizer",MB_ICONINFORMATION);
	}
		
		
				/*Soup/Salad*/
			if(LOWORD(wParam)==6)
			{
					cost=cost+4;
					sprintf(sum,"CAESAR\nCost: $4.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==7)
			{
					cost=cost+3.5;
					sprintf(sum,"GARDEN\nCost: $3.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==8)
			{
					cost=cost+5;
					sprintf(sum,"CHILI\nCost: $5.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==9)
			{
					cost=cost+4;
					sprintf(sum,"CHICKEN\nCost: $4.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==10)
			{
					cost=cost+3.5;
					sprintf(sum,"TOMATO\nCost: $3.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
	}
			
			/*Entree*/
			if(LOWORD(wParam)==11)
			{
					cost=cost+8;
					sprintf(sum,"BURGER\nCost: $8.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Soup/Salad",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==12)
			{
					cost=cost+7.5;
					sprintf(sum,"CLUB SANDWICH\nCost: $7.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Entree",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==13)
			{
					cost=cost+12;
					sprintf(sum,"MEATLOAF\nCost: $12.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Entree",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==14)
			{
					cost=cost+9;
					sprintf(sum,"FRIED CHICKEN\nCost: $9.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Entree",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==15)
			{
					cost=cost+12;
					sprintf(sum,"LASAGNA\nCost: $12.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Entree",MB_ICONINFORMATION);
}
			
				/*Dessert*/
			if(LOWORD(wParam)==16)
			{
					cost=cost+7;
					sprintf(sum,"CAKE\nCost: $7.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Dessert",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==17)
			{
					cost=cost+7;
					sprintf(sum,"APPLE PIE\nCost: $7.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Dessert",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==18)
			{
					cost=cost+6.5;
						sprintf(sum,"BREAD PUDDING\nCost: $6.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Dessert",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==19)
			{
					cost=cost+6;
						sprintf(sum,"ICE CREAM\nCost: $6.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Dessert",MB_ICONINFORMATION);
}
			if(LOWORD(wParam)==20)
			{
					cost=cost+5.5;
						sprintf(sum,"COOKIES\nCost: $5.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Dessert",MB_ICONINFORMATION);
	}
			
			/*Drink*/
			if(LOWORD(wParam)==21)
			{
					cost=cost+2;
					sprintf(sum,"COKE\nCost: $2.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Drink",MB_ICONINFORMATION);		
	}
			if(LOWORD(wParam)==22)
			{
					cost=cost+2;
						sprintf(sum,"SPRITE\nCost: $2.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Drink",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==23)
			{
					cost=cost+2;
						sprintf(sum,"TEA\nCost: $2.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Drink",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==24)
			{
						cost=cost+2.5;
							sprintf(sum,"COFFEE\nCost: $2.50\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Drink",MB_ICONINFORMATION);
	}
			if(LOWORD(wParam)==25)
			{
				cost=cost+3;
					sprintf(sum,"SHAKE\nCost: $3.00\n Your current total is: $%.2f",cost);
				MessageBox(hwnd, sum, "Drink",MB_ICONINFORMATION);
			}
		
			/*Controls*/
			if(LOWORD(wParam)==26)
			{
				//cost=cost+3;
				sprintf(sum,"Your final total is: $%.2f.\nThank you for your business.",cost);
				MessageBox(hwnd, sum, "Order Confirmed",MB_ICONINFORMATION);
				cost=0;
				//_snprintf(outStr, "Your total is; $%.2f\n", cost);//
			}
			if(LOWORD(wParam)==27)
			{
				cost=0;
              MessageBox(NULL,"Your transaction has been reset/cancelled.\nPlease select a new item.","New Transaction",MB_ICONINFORMATION);
			}
			break;			
		
			
		
			
			// also use switch//
		}
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}


/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    WNDCLASSEX wc; /* A properties struct of our window */
    HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
    MSG Msg; /* A temporary location for all messages */

    /* zero out the struct and set the stuff we want to modify */
    memset(&wc,0,sizeof(wc));
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.lpfnWndProc = WndProc; /* This is where we will send messages to */
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);

    /* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+0);
    wc.lpszClassName = "WindowClass";
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

    if(!RegisterClassEx(&wc)) {
        MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }
    //main window options
    hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Davis House of Cuisine",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT, /* x */
    CW_USEDEFAULT, /* y */
    700, /* width */
    600, /* height */
    NULL,NULL,hInstance,NULL);

    if(hwnd == NULL) {
        MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
        return 0;
    }

    /*
    This is the heart of our program where all input is processed and
    sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
    this loop will not produce unreasonably high CPU usage
    */
    while(GetMessage(&Msg, NULL, 0, 0) > 0) { /* If no error is received... */
        TranslateMessage(&Msg); /* Translate key codes to chars if present */
        DispatchMessage(&Msg); /* Send it to WndProc */
    }

    return Msg.wParam;
}